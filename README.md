Scraping and processing Water System Detail data from HTML pages.

# Installation

First, clone the Git repository:

```sh
git clone <URL> && cd sdwis-scraping
```

Then, install the dependencies using Conda:

```sh
NAME='sdwis-scraping' && conda env create --name $NAME --file environment.yml && source activate $NAME
```
